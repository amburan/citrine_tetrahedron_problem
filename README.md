# HOOMD-blue example scripts

Welcome to the HOOMD-blue example scripts. These jupyter notebooks demonstrate how to utilize the
functionality of [HOOMD-blue](http://glotzerlab.engin.umich.edu/hoomd-blue/). You can view a
[static version of the notebooks online](http://nbviewer.jupyter.org/urls/bitbucket.org/amburan/citrine_tetrahedron_problem/raw/1616d40e9bd931e672cf3aac17e08b5989905000/index.ipynb).
To download and execute these notebooks locally:
First install anaconda. It is recommended to install its light weight version [miniconda](https://conda.io/miniconda.html) instead.

```
git clone https://bitbucket.org/amburan/citrine_tetrahedron_problem.git
cd citrine_tetrahedron_problem
conda env create -f environment.yml
source activate citrine_challenge
jupyter notebook index.ipynb
```
**Search for "Quick compression scheme for hard tetrahedrons" within the index page.**

These examples use the following python packages. If you do not have the packages installed, some or all of the examples
will fail to execute: [HOOMD-blue](http://glotzerlab.engin.umich.edu/hoomd-blue/), [Jupyter](http://jupyter.org/), [GSD](https://bitbucket.org/glotzer/gsd), [matplotlib](http://matplotlib.org/), [freud](http://glotzerlab.engin.umich.edu/freud/), [fresnel](https://bitbucket.org/glotzer/fresnel), and [pillow](https://python-pillow.org/).

See [Notebook Basics](http://nbviewer.jupyter.org/github/jupyter/notebook/blob/master/docs/source/examples/Notebook/Notebook%20Basics.ipynb) and [Running Code](http://nbviewer.jupyter.org/github/jupyter/notebook/blob/master/docs/source/examples/Notebook/Running%20Code.ipynb) for tutorials on using jupyter itself.
